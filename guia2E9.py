import random


def crearmatriz():
    n = random.randint(3, 10)
    matriz = []
    # for que rellenara la matriz de ceros.
    for i in range(n):
        matriz.append([])
        for j in range(n):
            matriz[i].append("0")
    # for que intercambiara los 0 de la diagonal central
    # por unos.
    for i in range(n):
        matriz[i][i] = 1
    for i in matriz:
        print(i)

crearmatriz()