import random
import time
def creador_listas():
    palabras = ["Devil", "may","Cry"]

    time.sleep(1.5)
    print("Primera lista")
    lista1 = []
    N = random.randint(6,10)
    # for que rellena la primera lista con palabras aleatorias
    # de la variable palabras.
    for i in range(N):
        lista1.append(random.choice(palabras))
    print(lista1)
    # ciclo de if que evaluan cual palabra esta mas repetida y la que esta menos repetida
    if lista1.count("Devil") > lista1.count("may") and lista1.count("Devil") > lista1.count("Cry"):
        print("Palabra mas repetida: ")
        print("Devil")
    if lista1.count("may") > lista1.count("Devil") and lista1.count("may") > lista1.count("Cry"):
        print("Palabra mas repetida: ")
        print("may")
    if lista1.count("Cry") > lista1.count("may") and lista1.count("Cry") > lista1.count("Devil"):
        print("Palabra mas repetida: ")
        print("Cry")
    if lista1.count("Devil") < lista1.count("may") and lista1.count("Devil") < lista1.count("Cry"):
        print("Palabra menos repetida: ")
        print("Devil")
    if lista1.count("may") < lista1.count("Devil") and lista1.count("may") < lista1.count("Cry"):
        print("Palabra menos repetida: ")
        print("may")
    if lista1.count("Cry") < lista1.count("may") and lista1.count("Cry") < lista1.count("Devil"):
        print("Palabra menos repetida: ")
        print("Cry")
    print("\n")
    time.sleep(1.5)
    print("Segunda lista")
    lista2 = []
    # for que rellena la segunda lista con palabras aleatorias
    # de la variable palabras.
    for i in range(N):
        lista2.append(random.choice(palabras))
    print(lista2)
    # ciclo de if que evaluan cual palabra esta mas repetida y la que esta menos repetida
    if lista2.count("Devil") > lista2.count("may") and lista2.count("Devil") > lista2.count("Cry"):
        print("Palabra mas repetida: ")
        print("Devil")
    if lista2.count("may") > lista2.count("Devil") and lista2.count("may") > lista2.count("Cry"):
        print("Palabra mas repetida: ")
        print("may")
    if lista2.count("Cry") > lista2.count("may") and lista2.count("Cry") > lista2.count("Devil"):
        print("Palabra mas repetida: ")
        print("Cry")
    if lista2.count("Devil") < lista2.count("may") and lista2.count("Devil") < lista2.count("Cry"):
        print("Palabra menos repetida: ")
        print("Devil")
    if lista2.count("may") < lista2.count("Devil") and lista2.count("may") < lista2.count("Cry"):
        print("Palabra menos repetida: ")
        print("may")
    if lista2.count("Cry") < lista2.count("may") and lista2.count("Cry") < lista2.count("Devil"):
        print("Palabra menos repetida: ")
        print("Cry")
    print("\n")
    time.sleep(1.5)
    print("Tercera lista")
    lista3 = []
    # for que rellena la tercera lista con palabras aleatorias
    # de la variable palabras.
    for i in range(N):
        lista3.append(random.choice(palabras))
    print(lista3)
    # ciclo de if que evaluan cual palabra esta mas repetida y la que esta menos repetida
    if lista3.count("Devil") > lista3.count("may") and lista3.count("Devil") > lista3.count("Cry"):
        print("Palabra mas repetida: ")
        print("Devil")
    if lista3.count("may") > lista3.count("Devil") and lista3.count("may") > lista3.count("Cry"):
        print("Palabra mas repetida: ")
        print("may")
    if lista3.count("Cry") > lista3.count("may") and lista3.count("Cry") > lista3.count("Devil"):
        print("Palabra mas repetida: ")
        print("Cry")
    if lista3.count("Devil") < lista3.count("may") and lista3.count("Devil") < lista3.count("Cry"):
        print("Palabra menos repetida: ")
        print("Devil")
    if lista3.count("may") < lista3.count("Devil") and lista3.count("may") < lista3.count("Cry"):
        print("Palabra menos repetida: ")
        print("may")
    if lista3.count("Cry") < lista3.count("may") and lista3.count("Cry") < lista3.count("Devil"):
        print("Palabra menos repetida: ")
        print("Cry")
    print("\n")
    # Se imprimen los elementos repetidos de cada una de las listas
    time.sleep(1.5)
    print("---------Elementos repetidos---------")
    print("Primera listra: ")
    print(" ".join(set(lista1)))
    print("\n")
    print("Segunda listra: ")
    print(" ".join(set(lista2)))
    print("\n")
    print("Tercera listra: ")
    print(" ".join(set(lista3)))
    print("--------------------------------------")

creador_listas()
