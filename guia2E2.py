def crearmatriz():
    tamano = int(input("Ingrese el tamaño: "))
    matriz = []
    # Ciclo de for que rellenaran el triangulo y luego vaciaran los datos
    # a partir de la mitad de la matriz hacia arriba para crear el triangulo
    for i in range(tamano):
        matriz.append([])
        for j in range(tamano*2):
            if j > i and j < tamano * 2-i-1:
                matriz[i].append(" ")
            else:
                matriz[i].append("*")
    for i in range(tamano):
        print(' '.join(matriz[i]))


crearmatriz()
