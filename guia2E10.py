import time
string = "¡XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt"
print("--------------DESENCRIPTANDO MENSAJE--------------")
print(string)
# uso de comando list para pasar la variable
# string a una lista y asi sea mas facil de manipular.
stringlista = list(string)
# Comando que recorrera la lista y se saltara espacios de dos en dos
# para quitar las X del mensaje encriptado.
mensaje = stringlista[0:49:2]
time.sleep(1.5)
print("".join(mensaje))
# Uso de comando reverse para invertir la lista.
mensaje.reverse()
time.sleep(1.5)
print("".join(mensaje))
time.sleep(1.5)
mensaje[24] = "!"
time.sleep(1.5)
print("".join(mensaje))
time.sleep(1.5)
print("--------------MENSAJE DESENCRIPTADO--------------")