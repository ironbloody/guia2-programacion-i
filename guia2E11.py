import random
import time
lista = []
# for que rellenara una lista de 5 elementos
# con numeros aleatorios del 0 hasta el 19.
for i in range(5):
    lista.append(random.randint(0, 20))
time.sleep(1.5)
print("-----Lista original-------")
time.sleep(1.5)
print("  ", lista)
time.sleep(1.5)
print("--------------------------")
time.sleep(1.5)
print("----Ordenando la lista----")
# cadena de for que cumpliran la funcion del comando sort()
for i in range(1, len(lista)):
    for j in range(0, len(lista)-i):
        if lista[j] > lista[j+1]:
            temp = lista[j]
            lista[j] = lista[j+1]
            lista[j + 1] = temp
time.sleep(1.5)
print("  ", lista)
time.sleep(1.5)
print("------Lista ordenada------")
