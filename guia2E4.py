import random
# Funcion que crea una lista y la rellena con numeros del 0 al 50
# y luego escoje un numero al azar para ver sus menores,mayores, igual
# y multiplos.


def lista_padre():
    print("Lista original")
    lista = [n for n in range(0, 51)]
    print(lista)
    k = random.randint(0, 50)
    print("Numero es: ", k)
    numeros_menores(lista, k)


def numeros_menores(lista, k):
    print("\nMenores: ")
    print(lista[0:k])
    print("Mayores: ")
    print(lista[k+1:51])
    print("iguales")
    print(lista[k:k + 1])
    print("Multiplos: ")
    print(lista[k::k])


lista_padre()
