import random
import time
a = [1, 2, 3]
b = [1, 5, 6]
c = [5, 6, 7]
time.sleep(1.5)
print("Lista completa")
sumadelistas = a + b + c
print(sumadelistas)
time.sleep(1.5)
print("---Eliminando elementos repetidos---")
time.sleep(1.5)
# Comando que elimina todos los elementos repetidos y
# y luego transforma los elementos restantes en una lista
a = list(set(sumadelistas))
print(a)
time.sleep(1.5)
print("------------------------------------")
time.sleep(1.5)
time.sleep(1.5)
# Los numeros pares se volveran aleatorios en un rango del 0 hasta 99
print("----creando aleatoriedad en pares----")
a[1] = random.randint(0, 100)
a[4] = random.randint(0, 100)
time.sleep(1.5)
print(a)
time.sleep(1.5)
print("-------------------------------------")
